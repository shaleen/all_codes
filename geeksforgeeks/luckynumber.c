#include<stdio.h>

int main()
{
    int t, a, i, ans;
    scanf("%d", &t);
    for(i=0; i<t; i++)
    {
        scanf("%d",&a);
        ans = a % 9;
        if(ans == 0)
            ans = 9;
        printf("%d\n", ans);
    }
    return 0;
}
