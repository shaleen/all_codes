Question
==

Problem Code: LuckyNumber

Problem Description:
Given a number find the sum of its digits recursivly till a single digit is obtained.

Input:
The first line of input is an integer 'T' denoting the number of test cases.The description of T test cases is as follows.
Each test case will contain an integer 'A' followed by a newline.

Output:
Print the single digit number for each test case followed by a newline character.

Constraints:
1 <= T <= 1000000
100 <= A <= 1000000
time = 0.2 secs

Example:
input:
1
1996
output:
7

Explaination: 1+9+9+6 = 25 , 2+5 = 7

Tags: algorithm, mathematics

Code URL: http://code.geeksforgeeks.org/wNnVV3
Test code URL: http://code.geeksforgeeks.org/5mhPT3
