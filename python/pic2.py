from moviepy.editor import VideoClip

def make_frame(t):
    """ returns a numpy array of the frame at time t """
    # ... here make a frame_for_time_t
    return frame_for_time_t

clip = VideoClip(make_frame, duration=3) # 3-second clip
clip.write_videofile("my_animation.mp4", fps=24) # export as video
clip.write_gif("my_animation.gif", fps=24) # export as GIF
