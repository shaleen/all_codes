#!/usr/bin/python

import smtplib

sender = 'random@students.iiit.ac.in'
receivers = ['shaleen.garg@students.iiit.ac.in']

message = """From: From Person <sender>
To: To Person <to@todomain.com>
Subject: SMTP e-mail test

This is a test e-mail message.
"""

try:
   smtpObj = smtplib.SMTP('localhost:8080')
   smtpObj.sendmail(sender, receivers, message)         
   print "Successfully sent email"
except SMTPException:
    print "Error: unable to send email"

