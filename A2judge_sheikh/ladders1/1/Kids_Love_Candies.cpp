#include<bits/stdc++.h>
using namespace std;
int main()
{
    int t, k, n, ans, a;
    cin >> t;
    while(t--)
    {
        cin >> n >> k;
        while(n--)
        {
            cin >> a;
            ans += a/k;
        }
        cout << ans << endl;
        ans = 0;
    }
    return 0;
}
