//266B  CodeForces
//http://codeforces.com/problemset/problem/266/B
#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n, t, i, point;
    char B = 'B';
    char G = 'G';
    cin >> n >> t;
    char A[50];
    cin >> A;
    while(t--)
    {
        for(i=0; i<n-1; i++)        
        {
            if(A[i] == 'B' && A[i+1] == 'G')
            {
                A[i] = 'G';
                A[i+1] = 'B';
                i +=1;
            }
        }
    }
        cout << A << endl;
    return 0;
}
