#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main()
{
    int n, t, i, point;
    scanf("%d%d", &n, &t);
    char A[n+1];
    scanf("%s", A);
    while(t--)
    {
        for(i=0; i<n-1; i++)
        {
            if(A[i] == 'B' && A[i+1] == 'G')
            {
                A[i] = 'G';
                A[i+1] = 'B';
                i += 1;
            }
        }
    }
    printf("%s\n",A);
    return 0;
}
