#include<iostream>
#include<vector>

using namespace std;

int main()
{
    vector<int> v;
    int i, a, b;
    cin >> i;
    while( i-- )
    {
        cin >> a;
        v.push_back(a);
    }
    cin >> i;
    v.erase(v.begin() + (i-1));
    cin >> a >> b;
    v.erase(v.begin() + (a-1), v.begin() + (b-1));
    a = 0;
    cout << v.size() << endl;
    while(a < v.size()) 
    {
        cout << v[a] << " ";
        a ++;
    }
    cout << endl;
    return 0;
}
