#include<stdio.h>
#include<curses.h>

int main()
{
    char init_msg[] = "Welcome to the GAME";
    int r, c;
    initscr();
    getmaxyx(stdscr, r, c);
    mvprintw(r/2, (c - strlen(init_msg))/2, "%s", init_msg);
    mvprintw(r-2, 0, "this screen has %d rows and %d columns",r, c);
    refresh();
    getch();
    endwin();
    return 0;
}
