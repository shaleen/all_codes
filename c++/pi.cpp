#include<iostream>
#include<cstdlib>
#include<ctime>
#include<cmath>

using namespace std;

int main()
{
	srand(time(0));
	double x, y, pi, d;
	int n, in_circle = 0,nn;
	cout << "Enter times to throw the dart : " ; 
	cin >> n;
	nn = n;
	while(n--)
	{
		x = (double)rand();
		x = (double)fmod(x,2);
		y = (double)rand();
		y = (double)fmod(y,2);
		x = x * x;
		y = y * y;
		x = x + y;
		d =(double)sqrt(x);
		if(d <= 1)
			in_circle += 1;	
	}
	cout << "in circle = " << in_circle << endl;
	pi = (double) (4 * in_circle)/nn;
	cout.precision(15);
	cout << "pi = " << pi << endl;
	return 0;
}
