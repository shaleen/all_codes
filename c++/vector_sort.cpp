//Simple Sort programme using Vectors https://www.hackerrank.com/challenges/vector-sort
#include<iostream>
#include<vector>
#include<algorithm>
#include<cmath>
#include<cstdio>

using namespace std;

int main()
{
    vector<int> v;
    int times, i, time;
    cin >> times;
    time = times;
    while(times --)
    {
        cin >> i;
        v.push_back(i);
    }
    sort(v.begin(), v.end());
    i = 0;
    while( i < time)
    {
        cout << v[i] << " " ;
        i++;
    }
    cout << endl;
    return 0;
}
