#include<iostream>
#include<ctime>
#include<cstdlib>

using namespace std;

int A_i, A_j;
int rand_num(int **A, int size)
{
    int i, j, sign, n;
    for(i = 0; i < size; i++)
        for(j = 0; j < size; j++)
        {
            sign = rand() % 2 ;
            n = (rand() % 9) + 1;
            if(sign == 1)
            {
                n = -n;
            }
            A[i][j] = n;
        }
}

void table(int **A, int i)
{
    int ROW, COL, j;
    ROW = i;
    COL = i;
    for(j = 0; j < ROW; j++)
    {
        cout << "\x1b[5m"<< "+" << "\x1b[0m";
        for (i = 0; i < COL; i++)
        {
            cout << " - - - " << "\x1b[5m"<< "+" << "\x1b[0m";
        }
        cout << endl;
        cout << "|" ;
        for (i = 0; i < COL; i++)
        {
            cout << "       |" ;
        }
        cout << endl;
        cout << "|";
        for (i = 0; i < COL; i++)
        {
            if(A[j][i] > 0)
            cout << "   " << A[j][i] << "   |";
            else 
                cout << "  " << A[j][i] << "   |" ;
        }
        cout << endl;
        cout << "|" ;
        for (i = 0; i < COL; i++)
        {
            cout << "       |" ;
        }
        cout << endl;
    }
    cout << "\x1b[5m" << "+" << "\x1b[0m";
    for (i = 0; i < COL; i++)
    {
            cout << " - - - " << "\x1b[5m"<< "+" << "\x1b[0m";
    }
    cout << endl;
}

int main()
{
    int size;
    cout << "\x1b[5m" << "This is GOOD !!" << "\x1b[0m" << endl;
    cout << "input size : " ;
    cin >> size;
    // Array malloc
    int** A = new int*[size];
    for(int i = 0; i < size; ++i)
        A[i] = new int[size];

    srand(time(NULL));
    rand_num(A, size);
    table(A, size);
    return 0;
}
