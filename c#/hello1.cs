// Hello3.cs
// arguments: A B C D
using System;

public class Hello3
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        int [] num = {1,2,3,4,5,6,7,8,9};
        foreach (int i in num)
        {
            Console.WriteLine("{0}", i);
        }
    }
}
