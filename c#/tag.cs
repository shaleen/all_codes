using System;
using Taglib;

public class TagsEdit
{
    public static void Main()
    {
        Console.WriteLine("Welcome to My Tag edit");

        TagLib.File file = TagLib.File.Create("/home/shaleen/Dropbox/codes/c#/music.mp3");
        string title = file.Tag.Title;
        uint track = file.Tag.Track;
        string album = file.Tag.Album;
        string [] artists = file.Tag.Artists; 
        Console.WriteLine("Title: {0}, Track: {0}, Album: {0}, Artists: {0}", title, track, album, artists[0]);
        Console.Write("Type the title name: ");
        title = Console.ReadLine();
        Console.Write("Type the track number: ");
        track = Console.ReadLine();
        Console.Write("Type the album name: ");
        album = Console.Readline();
        // Not implemented writing artists
        //now writing the mp3 file
        file.Tag.Title = title;
        file.Tag.Track = track;
        file.Tag.Album = album;
        // Checking if changes done
        title = file.Tag.Title;
        track = file.Tag.Track;
        album = file.Tag.Album;
        Console.WriteLine("Title: {0}, Track: {0}, Album: {0}, Artists: {0}", title, track, album, artists[0]);
        file.Save();
    }
}
