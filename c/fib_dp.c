#include<stdio.h>
#define NIL -1
#define MAX 100

int lookup[MAX];

void init()
{
    int i;
    for( i = 0; i <= MAX; i++)
        lookup[i] = NIL;
}

int fib( int n )
{
    if(n < 0)
        return 0;
    if(lookup[n] == NIL)
    {
        if(n <= 1)
            lookup[n] = n;
        else
            lookup[n] = fib(n-1) + fib (n-2);
    }
    return lookup[n];
}

int main()
{
    int i;
    init();
    while(1)
    {
        scanf("%d",&i);
        printf("the fibbonaci number is %d\n",fib(i));
    }
    return 0;
}
