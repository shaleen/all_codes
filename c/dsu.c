#include <stdio.h>
#define MAX 1000001

typedef struct {
    int P[MAX], R[MAX];
}dsd;

typedef struct {
    int u,v,w;
}edge;

edge E[MAX];
int tot_w;

void init_dsd(dsd *A, int N)
{
    int i;
    for(i=1; i<=MAX; i++)
        A->P[i] = i;
}

void merge(dsd *A, int x, int y, int w)
{
    int u, v;
    u = find(A, x);
    v = find(A, y);

    if(u==v)
        return ;
    if(A->R[u] == A->R[v])
    {
        A->P[u] = v;
        A->R[v]++;
    }
    else if(A->R[u] > A->R[v])
        A->P[v] = u;
    else
        A->P[u] = v;
    tot_w+=w;
}

int find(dsd *A, int x)
{
    if(A->P[x] == x)
        return x;

    A->P[x] = find(A, A->P[x]);
    return A->P[x];
}

int comp(const void *a, const void *b)
{
    edge *x, *y;
    x=(edge*)a;
    y=(edge*)b;
    return (x->w - y->w);
}

int main()
{
    int N, M, i;
    dsd A;
    scanf("%d%d", &N, &M);
    for(i=0; i<M; i++)
        scanf("%d%d%d", &E[i].u, &E[i].v, &E[i].w);
    qsort(E, M, sizeof(edge), comp);
    init_dsd(&A, N);   
    i=0;
    while(i<M)
    {
        merge(&A, E[i].u, E[i].v, E[i].w);      
        i++;
    }
    printf("%d\n",tot_w);
    return 0;
}
