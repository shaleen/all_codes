#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 1000001

typedef struct b{
    int v;
    struct b* next;
}node;

node A1[MAX],A2[MAX];
int visit[MAX],adj[MAX],stack[MAX],stk_ptr=-1,visited;

void push();
int pop();
void adj_list(int n,int tu);

void insert(int i,int j, int a)
{
    node*point,*new,*nxt;
    new=malloc(sizeof(node));
    new->v=j;
    new->next=NULL;
    if(a==1)
        point=&A1[i];
    else
        point=&A2[i];
    nxt=point->next;
    point->next=new;
    new->next=nxt;
}

void dfs(int root , int tu)
{
    int i,a,b=0;
    visited++;
    visit[root]=1;
    adj_list(root,tu);
    push();
    printf("stk_ptr=%d\n",stk_ptr);
    while(stk_ptr!=-1)
    {
        a=pop();
        printf("Stack= ");
        while(b<=stk_ptr)
        {
            printf("%d ",stack[b]);
            b++;
        }
        
        if(visit[a]==0)
        {
//            visited++;
            adj_list(a,tu);
            visit[a]=1;
            push();
        }
    }
}

void adj_list(int n,int tu)
{
    int i=0;
    node *point;
    if(tu==1)
        point=&A1[n];
    else
        point=&A2[n];
    point=point->next;
    //printf("point=%d\n",point->v);
    while(point!=NULL)
    {
        if(visit[point->v]==0)
        {
            adj[i]=point->v;
            point=point->next;
            i++;
            visited++;
        }
        else
            point=point->next;
    }
}

void push()
{
    int i;
    while(adj[i]!=0)
    {
        stk_ptr++;
        stack[stk_ptr]=adj[i];
        i++;
    }
}

int pop()
{
    stk_ptr--;
    return(stack[stk_ptr+1]);
}
int main()
{
    int t,N,i,M,j,loop;
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d",&N);
        for(loop=1;loop<=N;loop++)
        {
            scanf("%d",&M);
            while(M--)
            {
                scanf("%d",&j);
                insert(j,loop,1);
                insert(loop,j,2);
            }
        }

        for(i=1;i<=N;i++)
        {
            node *temp=&A1[i];
            temp=temp->next;
            printf("i=%d\n",i);
            while(temp!=NULL)
            {
                printf("A1-%d",temp->v);
                temp=temp->next;
        }
            printf("\n");
        }
        for(loop=1;loop<=N;loop++)
        {
            memset(visit,0,sizeof(int));
            memset(stack,0,sizeof(int));
            stk_ptr=-1;
            visited=1;
            dfs(loop,1);
            printf("visited=%d\n",visited);
            if(visited==N)
            {
                visited=1;
                dfs(loop,2);
                printf("hello\n");
                printf("%d\n",visited);
                break;
            }
        }
    }
    return 0;
}
