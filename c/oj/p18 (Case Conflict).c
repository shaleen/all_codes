#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 1000001
typedef struct b{
    int v;
    struct b* next;
}node;

node A[MAX],B[MAX];
int visited[MAX],adj[MAX],stack[MAX],stk_ptr=-1,v=0;

void print_stk();
void pushh();
void insert1 (int i,int j)
{
    node *point,*new,*nxt;
    new=malloc(sizeof(node));
    new->v=j;
    new->next=NULL;
    point=&A[i];
    nxt=point->next;
    point->next=new;
    new->next=nxt;
}
void insert2 (int i,int j)
{
    node *point,*new,*nxt;
    new=malloc(sizeof(node));
    new->v=j;
    new->next=NULL;
    point=&B[i];
    nxt=point->next;
    point->next=new;
    new->next=nxt;
}

void adj_list(int a,int ty)
{
    int i=0;
    node *point;
    if(ty==1)
        point=&A[a];
    else if(ty==2)
        point=&B[a];
    point=point->next;
    memset(adj,0,sizeof(int));
    while(point!=NULL)
    {
        if(visited[point->v]==0)
        {
            adj[i]=point->v;
            point=point->next;
            i++;
        }
        else
        {
            point=point->next;
        }
    }
}

void dfs(int root,int ty)
{
    int i,a;
    printf("%d\n",root);
    visited[root]=1;
    adj_list(root,ty);
    pushh();
    while(stk_ptr!=-1)
    {
        a=pop();
        if (visited[a]==0)
        {
            printf("%d\n",a);
            v++;
            visited[a]=1;
            adj_list(a,ty);
            pushh();
        }
    }
}

void pushh()
{
    int i=0;
    while(adj[i]!=0)
    {
        stk_ptr++;
        stack[stk_ptr]=adj[i];
        i++;
    }
}

int pop()
{
    stk_ptr--;
    return stack[stk_ptr+1];
}

int main()
{
    int a,i,b,test,N,M;
    scanf("%d",&test);
    while(test--)
    {
        scanf("%d",&N);
        for(b=1;b<=N;b++)
        {
            scanf("%d",&M);
            while(M--)
            {
                scanf("%d",&i);
                insert1(i,b);
                insert2(b,i);
            }
        }
        for(i=1;i<=N;i++)
        {
            v=0;
            for(i=1;i<=N;i++)
                visited[i]=0;
            dfs(i,2);
            if(v==N)
            {
                v=0;
                dfs(i,1);
                printf("%d\n",v);
                break;
            }
        }
    }
    return 0;
}
