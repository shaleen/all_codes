#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 1000001

typedef struct b{
    int v;
    struct b* next;
}node;

node A[MAX];
int visited[MAX],adj[MAX],stack[MAX],stk_ptr=-1,max,no,par[MAX],dist[MAX];

void print_stk();
void push();

void insert(int i,int j)
{
    node *point,*new,*nxt;
    new=malloc(sizeof(node));
    new->v=j;
    new->next=NULL;
    point=&A[i];
    nxt=point->next;
    point->next=new;
    new->next=nxt;
}

void print_adjlist(int N){
    int i;
    node *t = NULL;
    for(i=1; i<=N; i++){
        t = &A[i];
        t = t->next;
        fprintf(stderr,"%d: ", i);
        while(t!=NULL){
            fprintf(stderr, "%d ", t->v);
            t = t->next;
        }
        fprintf(stderr, "\n");
    }
}

void adj_list(int a)
{
    int i=0;
    node *point=&A[a];
    point=point->next;
    memset(adj,0,sizeof(int));
    while(point!=NULL)
    {
        //printf("visited[%d]=%d",point->v,visited[point->v]);
        if(visited[point->v]==0)
        {
            adj[i]=point->v;
            par[point->v]=a;
            point=point->next;
            i++;
        }
        else
        {
            //   printf("else\n");
            point=point->next;
        }
    }
}

void check(int node)
{
    if(dist[node]>max)
    {
        max=dist[node];
        no=node;
        //       fprintf(stderr, "MAX=%d no=%d\n",max,no);

    }
}

void dfs(int root)
{
    int i,a;
    dist[root]=1;
    check(root);
    visited[root]=1;
    adj_list(root);
    push();
    while(stk_ptr!=-1)
    {
        //       fprintf(stderr, "stk_ptr=%d\n",stk_ptr);
        a=pop();
        //     fprintf(stderr, "visiting %d, d[a] = %d\n", a, dist[a]);
        if (visited[a]==0)
        {
            dist[a]=dist[par[a]]+1; 
            //           fprintf(stderr, "p[%d] %d\n", a, par[a]);
            //         fprintf(stderr, "%d, %d\n", dist[par[a]]+1, dist[a]);
            check(a);
            visited[a]=1;
            adj_list(a);
            push();
        }
    }
}

void push()
{
    int i=0;
    while(adj[i]!=0)
    {
        stk_ptr++;
        stack[stk_ptr]=adj[i];
        i++;
    }
    //   printf("push=%d\n",i);
}

int pop()
{
    stk_ptr--;
    return stack[stk_ptr+1];
}

int main()
{
    int a,i,j,b,t,root,loop;
    scanf("%d",&t);
    while(t--)
    {
        //RESET VAL
        max=0;
        for(loop=0;loop<MAX;loop++)
        {
            visited[loop]=0;
            A[loop].next=NULL;
            stack[loop]=0;
            dist[loop]=0;
            par[loop]=0;
        }
        stk_ptr=-1;
        //RESET VALUES

        scanf("%d",&b);
        scanf("%d%d",&i,&j);
        insert(i,j);
        insert(j,i);
        root=i;
        for(loop=0; loop<b-2; loop++)
        {
            scanf("%d%d",&i,&j);
            insert(i,j);
            insert(j,i);
        }
        print_adjlist(b);
        dfs(root);
        //RESET VAL
        fprintf(stderr, "MAX=%d\n",max);
        fprintf(stderr, "2nd DFS\n");
        max=0;
        for(loop=0;loop<MAX;loop++)
        {
            visited[loop]=0;
            stack[loop]=0;
            dist[loop]=0;
            par[loop]=0;
        }
        stk_ptr=-1;
        //RESET VALS
        dfs(no);
        printf("max=%d\n", max);
    }
    return 0;
}

