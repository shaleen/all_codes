#include<stdio.h>
#include<stdlib.h>

typedef struct P{
    int val;
    struct P *left, *right;
}node;

node *root ,*parent;
void insert(node *A, int n)
{
    node *point, *par;
    point = A;
    while(point != NULL)
    {
        par = point;
        if(n > point->val)
            point = point->right;
        else
            point = point->left; 
    }
    if(n > par->val)
    {
        par->right = malloc(sizeof(node)); 
        par = par->right;
        par->val = n;
    }
    else
    {
        par->left = malloc(sizeof(node));
        par = par->left;
        par->val = n;
    }
}

void inorder(node *A)
{
    node *pointer = A;
    if(pointer == NULL)
        return ;
    inorder(pointer->left);
    printf("%d ",pointer->val);
    inorder(pointer->right);
}

void preorder(node *A)
{
    node *pointer = A;
    if(pointer == NULL)
        return ;
    printf("%d ",pointer->val);
    preorder(pointer->left);
    preorder(pointer->right);
}

void postorder(node *A)
{
    node *pointer = A;
    if(pointer == NULL)
        return ;
    postorder(pointer->left);
    postorder(pointer->right);
    printf("%d ", pointer->val);
}

void transplant(node *A, node *u, node *v)
{
    node *pointer;
    parent = NULL;
    pointer = A;
    while(pointer != NULL)
    {
        if(pointer->val == u->val)
            break;
        parent = pointer;
        if(pointer->val < u->val)
            pointer = pointer->right;
        else
            pointer = pointer->left;
    }
    if(parent == NULL)
    {
        // root = v;
        // A = root;
        A = v;
    }
    else if(parent->left == u)
        parent->left = v;
    else
        parent->right = v;
}

void delete(node *A, int n)
{
    node *point_n, *par_n;
    point_n = A;
    while(point_n != NULL)
    {
        if(point_n->val == n)
            break;
        par_n = point_n;
        if(point_n->val < n)
            point_n = point_n->right;
        else
            point_n = point_n->left;
    }
    if(point_n->left == NULL)
        transplant(A, point_n, point_n->right);
    else if(point_n->right == NULL)
        transplant(A, point_n, point_n->left);
    else
    {
        node *p2 = point_n->right, *par_p2 = NULL;
        while(p2->left != NULL)
        {
            par_p2 = p2;
            p2 = p2->left;
        }
        if(par_p2 == NULL)
            par_p2 = point_n;
        if(par_p2 != point_n)
        {
            par_p2->left = p2->right;
            point_n->val = p2->val;
        }
        else if(par_p2 == point_n)
        {
            point_n->right = p2->right;
            point_n->val = p2->val;
        }
    }
}        

int main()
{
    root = malloc(sizeof(node));
    scanf("%d",&root->val);
    int i;
    while(1)
    {
        scanf("%d", &i);
        if(i == 1)
        {
            scanf("%d",&i);
            insert(root, i);
        }
        else if(i == 2)
        {
            inorder(root);
            printf("\n");
        }
        else if(i == 4)
        {
            preorder(root);
            printf("\n");
        }
        else if(i == 5)
        {
            scanf("%d", &i);
            //            printf("i= %d\n",i);
            delete(root, i);
        }
    }
    return 0;
}
