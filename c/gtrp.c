#include<stdio.h>
#include<stdlib.h>

typedef struct P{
    int val;
    struct P *left, *right;
}node;

node *root ,*parent;

node *tree2list(node *root)
{
    node *a, *b;
    if(root == NULL)
        return NULL;
    a = tree2list(root->left);
    b = tree2list(root->right);
    root->left = root;
    root->right = root;
    a = append(a, root);
    a = append(a, b);
    return a;
}

node *append(node *a, node *b)
{
    node *alast, *blast;
    if(a == NULL)
        return b;
    if(b == NULL)
        return a;
    alast = a->left;
    blast = b->left;
    join(alast, b);
    join(blast, a);
    return a;
}

void join (node *a, node *b)
{
    a->right = b;
    b->left = a;
}
void insert(node *A, int n)
{
    node *point, *par;
    point = A;
    while(point != NULL)
    {
        par = point;
        if(n > point->val)
            point = point->right;
        else
            point = point->left; 
    }
    if(n > par->val)
    {
        par->right = malloc(sizeof(node)); 
        par = par->right;
        par->val = n;
    }
    else
    {
        par->left = malloc(sizeof(node));
        par = par->left;
        par->val = n;
    }
}

void inorder(node *A)
{
    node *pointer = A;
    if(pointer == NULL)
        return ;
    inorder(pointer->left);
    printf("%d ",pointer->val);
    inorder(pointer->right);
}

void printlist(node *head)
{
    node *c = head;
    while(c != NULL)
    {
        printf("%d ",c->val);
        c = c->right; 
        if(c == head)
            break;
    }
    printf("\n");
}
int main()
{
    node *head;
    root = malloc(sizeof(node));
    scanf("%d",&root->val);
    int i;
    while(1)
    {
        scanf("%d", &i);
        if(i == 1)
        {
            scanf("%d",&i);
            insert(root, i);
        }
        else if(i == 2)
        {
            inorder(root);
            printf("\n");
        }
        else if(i == 3)
        {
            head = tree2list(root);
            printlist(head);
        }
    }
    return 0;
}
