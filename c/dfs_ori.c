#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct b{
    int v;
    struct b* next;
}node;

node A[1000001];
int visited[1000001],adj[1000001],stack[1000001],stk_ptr=-1;

void print_stk();
void pushh();
void insert (int i,int j)
{
    node *point,*new,*nxt;
    new=malloc(sizeof(node));
    new->v=j;
    new->next=NULL;
    point=&A[i];
    nxt=point->next;
    point->next=new;
    new->next=nxt;
}

void adj_list(int a)
{
    int i=0;
    node *point=&A[a];
    point=point->next;
    memset(adj,0,sizeof(int));
    while(point!=NULL)
    {
        if(visited[point->v]==0)
        {
            adj[i]=point->v;
            point=point->next;
            i++;
        }
        else
        {
            point=point->next;
        }
    }
}

void dfs(int root)
{
    int i,a;
    printf("%d\n",root);
    visited[root]=1;
    adj_list(root);
    pushh();
    while(stk_ptr!=-1)
    {
        a=pop();
        if (visited[a]==0)
        {
            printf("%d\n",a);
            visited[a]=1;
            adj_list(a);
            pushh();
        }
    }
}

void pushh()
{
    int i=0;
    while(adj[i]!=0)
    {
        stk_ptr++;
        stack[stk_ptr]=adj[i];
        i++;
    }
}

int pop()
{
    stk_ptr--;
    return stack[stk_ptr+1];
}

int main()
{
    int a,i,j,b;
    scanf("%d",&b);
    while(b--)
    {
        scanf("%d%d",&i,&j);
        insert(i,j);
        insert(j,i);
    }
    dfs(i);
    return 0;
}
