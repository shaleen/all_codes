#include<stdio.h>
#include<stdlib.h>

typedef struct b{
	int v;
	struct b* next;
}node;
node A[1000001];

void insert (int i,int j)
{
	node *point,*new,*nxt;
	new=malloc(sizeof(node));
	new->v=j;
	new->next=NULL;
	point=&A[i];
	nxt=point->next;
	point->next=new;
	new->next=nxt;
}

void print(int a)
{
	node *point=&A[a];
	point=point->next;
	printf("%d : ",a);
	while(point!=NULL)
	{
		printf("%d ",point->v);
		point=point->next;
	}
	printf("\n");
}

int main()
{
	int a,i,j,b;
	scanf("%d%d",&a,&b);
	while(b--)
	{
		scanf("%d%d",&i,&j);
		insert(i,j);
		insert(j,i);
	}
	while(a>0)
	{
		print(a);
		a--;
	}
	return 0;
}

