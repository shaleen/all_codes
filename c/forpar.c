#include <stdio.h>
#include <omp.h>

int main(int argc, char **argv) 
{
    int  thread_id;
    long long int i,nloops;
#pragma omp parallel private(thread_id, nloops) 
    { 
        nloops = 0;
#pragma omp for
        for (i=0; i<10000000000000; ++i) 
        { 
            ++nloops; 
        } 
        thread_id = omp_get_thread_num(); 
        printf("Thread %d performed %lld iterations of the loop\n",thread_id,nloops);
    }
    return 0; 
} 
