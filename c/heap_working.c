//MIN HEAP
#include<stdio.h>
#include<limits.h>
#define MAX 1000000

int point = 0;

void swap(int A[], int p1, int p2)
{
    int temp;
    temp = A[p1];
    A[p1] = A[p2];
    A[p2] = temp;
}

void insert(int A[], int n)
{
    point++;
    A[point] = n;
    int par_n, p_n = point;
    while(p_n != 0 )
    {
        par_n = p_n/2;
        if(A[par_n] > A[p_n]) 
        {
            swap(A, par_n, p_n);
            p_n = par_n;
        }
        else 
            break;
    }
}

void delete(int A[], int n)
{
    int a=0;
    while(a <= point)
    {
        if(A[a] == n)
            break;
        else
            a++;
    }
    A[a] = A[point];
    point --;
    int child = a*2;
    while(child+1 <= point)
    {
        if(A[a] > A[child] || A[a] > A[child+1])
        {
            if(A[a] > A[child] && A[a] < A[child+1])
            {
                swap(A, child, a);
                a = child;
            }
            else if(A[a] < A[child] && A[a] > A[child+1])
            {
                swap(A, child+1, a);
                a = child +1;
            }
            else
            {
                if(A[child] < A[child+1])
                {
                    swap(A, child, a);
                    a = child;
                }
                else
                {
                    swap(A, child+1, a);
                    a = child+1;
                }
            }
            child = a*2;
        }
        else 
            break;
    }
}
int main()
{
    int A[MAX];
    A[0] = INT_MIN;
    int a;
    for(a = 1; a < MAX; a++)
        A[a] = 0;
    while(1)
    {
        scanf("%d",&a);
        if(a == 1)
        {
            scanf("%d",&a);
            insert(A, a);
        }
        else if(a == 2)
            printf("%d\n",A[1]);

        else if(a == 3)
        {
            scanf("%d",&a);
            printf("a= %d\n",a);
            delete(A, a);
        }
        else if (a == 4) 
        {
            for (a=1; a<=point; a++)
                printf("%d ",A[a]);
            printf("\n");
        }
    }
    return 0;
}
